const Pool = require('pg').Pool

const pool = new Pool({
    user: process.env.PGUSER,
    host: process.env.PGHOST,
    port: process.env.PGPORT,
    database: 'keycloak',
    password: process.env.PGPASSWORD
});

function getUser(idUser) {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT * 
            FROM public.user_entity 
            WHERE id=$1`,
            [idUser],          
            (error, results) => {
                if (error) {
                    reject(error);
                }
                resolve(results.rows[0])
            }
        )
    })
}

module.exports = {
    getUser
}