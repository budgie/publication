const Pool = require('pg').Pool

const pool = new Pool();

const users = require('./users')

/**
 * Get all posts order by creation date
 * 
 * @param request 
 * @param response 
 */
const getPosts = (request, response) => {
    pool.query('SELECT * FROM posts ORDER BY creation_date desc', (error, results) => {
        if (error) {
            response.status(404).json(error);
            throw error
        }        


        let updateRows = results.rows.map(element => {
            return new Promise((resolve, reject) => {      
                users.getUser(element.iduser).then(
                    (success) => {
                        console.log(success)
                        element.user = success
                        resolve()
                    },
                    (error) => {
                        throw error
                        reject()
                    }
                )
            })
        });

        Promise.all(updateRows)
        .then(() => {
            response.status(200).json(results.rows)
        })
        .catch(err => console.log(err))
    })
}

/**
 * Get post by id
 * 
 * @param request 
 * @param response 
 */
const getPostById = (request, response) => {
    const id = request.params.id

    pool.query('SELECT * FROM posts WHERE id = $1', [id], (error, results) => {
        if (error) {
            response.status(404).json(error);
            throw error
        } else if(id === null) {
            response.status(400).json(error);
        }
        response.status(200).json(results.rows)
    })
}

/**
 * Get post by user id and order by creation date 
 *
 * @param request 
 * @param response 
 */
const getPostByUser = (request, response) => {
    const user = request.params.idUser

    pool.query('SELECT * FROM posts WHERE iduser = $1 ORDER BY creation_date desc', [user], (error, results) => {
        if (error) {
            response.status(404).json(error);
            throw error
        } else if(user === null) {
            response.status(400).json(error);
        }

        let updateRows = results.rows.map(element => {
            return new Promise((resolve, reject) => {      
                users.getUser(element.iduser).then(
                    (success) => {
                        console.log(success)
                        element.user = success
                        resolve()
                    },
                    (error) => {
                        throw error
                        reject()
                    }
                )
            })
        });

        Promise.all(updateRows)
        .then(() => {
            response.status(200).json(results.rows)
        })
        .catch(err => console.log(err))
    })
}

/**
 * Create a new post
 * 
 * @param request 
 * @param response 
 */
const createPost = (request, response) => {
    const { idUser, message } = request.body

    pool.query('INSERT INTO posts (id, idUser, message) VALUES (uuid_generate_v1(), $1, $2) RETURNING id', [idUser, message], (error, results) => {
        if (error) {
            response.status(405).json(error);
            throw error
        }
        
        response.status(201).json(results.rows[0])
    })
}

/**
 * Update an existing post
 * 
 * @param request 
 * @param response 
 */
const updatePostMessage = (request, response) => {
    const { id, message } = request.body

    pool.query('UPDATE posts SET message = $1 WHERE id = $2', [message, id], (error, results) => {
        if (error) {
            response.status(404).json(error);
            throw error
        } else if(id === null) {
            response.status(400).json(error);
        }
        response.status(200).json({message : message})
    })
}

/**
 * Delete a post in the database
 * 
 * @param request 
 * @param response 
 */
const deletePost = (request, response) => {
    const id = request.params.id

    pool.query('DELETE FROM posts WHERE id = $1', [id], (error, res) => {
        if (error) {
            response.status(500).json(error);
            throw error
        } else if(id === null) {
            response.status(400).json(error);
        }
        response.status(200).json({id: id})
    })
}

module.exports = {
    getPosts,
    getPostById,
    getPostByUser,
    createPost,
    updatePostMessage,
    deletePost
}