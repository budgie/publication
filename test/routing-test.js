const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const should = chai.should();
const app = require('../index.js');

chai.use(chaiHttp);

describe("Posts", () => {
    //Test get route
    describe("GET /posts", () => {
            //Get all posts
            it('should get all posts record', (done) => {
                chai.request(app)
                  .get('/posts')
                  .end((err, res) => {
                    res.should.have.status(200);
                    done();
                  });
              });

            //Get a post by id
            it("should get a single post by id", (done) => {
                const id = "2014566e-dc0e-465f-9332-e83657a1b4a5";
                chai.request(app)
                .get(`/posts/${id}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
            });

            //Get a post by user
            it("should get a single post by user", (done) => {
                const user = "2713aaaf-29fe-4568-8830-cf92a19f8da1";
                chai.request(app).
                get(`/posts/${user}`).
                end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
            });

        })
        
        /*
        //Test post route
        describe('/POST /posts', () => {
            it('should not create a post without id', (done) => {
                const post = {
                    iduser : '2713aaaf-29fe-4568-8830-cf92a19f8da5',
                    message : 'My message'
                }
                console.log(post.iduser);
                chai.request(app).post('/posts').send(post).end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('id');
                    res.body.should.have.property('iduser');
                    res.body.should.have.property('message');
                    done();
                });
            })
        })
        */

        describe('/PUT/:id /posts', () => {
            it('it should UPDATE a post given the id', (done) => {
                const post = {
                    id : '2014566e-dc0e-465f-9332-e83657a1b4a5',
                    iduser : 'cbaa948a-232b-4ac9-bb23-e1c1300e02c5',
                    message : 'Premier message de Budgie'
                }
                chai.request(app)
                .put('/posts/' + post.id)
                .send({id: '2014566e-dc0e-465f-9332-e83657a1b4a5', message : post.message})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql(`Post modified with ID: ${post.id}`);
                    done();
                });
            });
        });
        
        describe('/DELETE/:id posts', () => {
            it('it should DELETE a post given the id', (done) => {
                const post = {
                    id : '2014566e-dc0e-465f-9332-e83657a1b4a5',
                    iduser : 'cbaa948a-232b-4ac9-bb23-e1c1300e02c5',
                    message : 'Premier message de Budgie'
                }
                chai.request(app)
                .delete('/posts/' + post.id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id').eql(`${post.id}`);
                    done();
                });
            });
        });
        
        
})

