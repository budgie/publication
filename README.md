# Publication
This service manage the post system in the Budgie project.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
### Prerequisites
#### Installing
To install dependancies run :
```sh
$ cd publication
$ npm install
```
### Configure
You have to configure the database access, start by creating a `.env` file in the root of the project :
```sh
$ touch .env
```
Add the following lines and add your database access :
```
IP=localhost
PORT=3001
PGUSER=
PGHOST=
PGPASSWORD=
PGDATABASE=
PGPORT=
```

### Running project
To run the project :
``` 
npm start 
```

Access the following url :
```
http://localhost:3001
```
You will have to login on Budgie app to access API route, to know existing route, access the following url :
```
http://localhost:3001/api-docs
```

### Running the tests
To run test, run the following command :
``` 
npm test 
```

### Authors
Budgie team
