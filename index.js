const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const db = require('./post')
const Pool = require('pg').Pool
const cors = require("cors");
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./swagger.yaml');
const Keycloak = require('keycloak-connect');
const session = require('express-session');

//Set up session with keycloak protection
var memoryStore = new session.MemoryStore();
var keycloak = new Keycloak({ store: memoryStore });

//session
app.use(session({
    secret:'thisShouldBeLongAndSecret',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));

require("dotenv").config()

const pool = new Pool();

app.use(cors());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

//Database connection
pool.connect(err => {
    if (err) {
        console.error('connection error', err.stack);
    } else {
        app.listen(process.env.PORT, process.env.IP, () => {
            console.log('Listening on ' + process.env.IP + ':' + process.env.PORT)
        });
    }
})

//Route protection
app.use(keycloak.middleware());

//GET POSTS
app.get('/posts', keycloak.protect(), db.getPosts)
//GET POST BY ID
app.get('/posts/:id', keycloak.protect(), db.getPostById)
//GET POST BY USER
app.get('/users/:idUser', keycloak.protect(), db.getPostByUser)
//CREATE POST
app.post('/posts', keycloak.protect(), db.createPost)
//UPDATE POST
app.put('/posts/:id', keycloak.protect(), db.updatePostMessage)
//DELETE POST
app.delete('/posts/:id', keycloak.protect(), db.deletePost)

app.use( keycloak.middleware( { logout: '/'} ));
